<?php

namespace App\Http\Controllers;

use App\Adapters\BarMovieServiceAdapter;
use App\Adapters\BazMovieServiceAdapter;
use App\Adapters\FooMovieServiceAdapter;
use External\Foo\Exceptions\ServiceUnavailableException as FooException;
use External\Baz\Exceptions\ServiceUnavailableException as BazException;
use External\Bar\Exceptions\ServiceUnavailableException as BarException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MovieController extends Controller
{
    private $fooMovieServiceAdapter;
    private $barMovieServiceAdapter;
    private $bazMovieServiceAdapter;

    public function __construct(FooMovieServiceAdapter $fooMovieServiceAdapter, BarMovieServiceAdapter $barMovieServiceAdapter, BazMovieServiceAdapter $bazMovieServiceAdapter)
    {
        $this->fooMovieServiceAdapter = $fooMovieServiceAdapter;
        $this->barMovieServiceAdapter = $barMovieServiceAdapter;
        $this->bazMovieServiceAdapter = $bazMovieServiceAdapter;
    }

    // getTitles method fetches movie titles from different services and merges them.
    public function getTitles(Request $request): JsonResponse
    {
        $allTitles = [];

        try {
            // Fetch and merge titles from Foo, Bar, and Baz services.
            $allTitles = array_merge($allTitles, $this->getTitlesWithRetry($this->fooMovieServiceAdapter));
            $allTitles = array_merge($allTitles, $this->getTitlesWithRetry($this->barMovieServiceAdapter));
            $allTitles = array_merge($allTitles, $this->getTitlesWithRetry($this->bazMovieServiceAdapter));
        } catch (FooException | BarException | BazException $e) {
            // If any service fails, return a failure response.
            return response()->json(['status' => 'failure'], 503);
        }

        return response()->json($allTitles);
    }

    // getTitlesWithRetry method attempts to fetch titles from a service with retries.
    private function getTitlesWithRetry($service, $retryCount = 3)
    {
        for ($i = 0; $i < $retryCount; $i++) {
            try {
                // Attempt to fetch titles.
                return $service->getTitles();
            } catch (FooException | BarException | BazException $e) {
                // If it's the last retry and the fetch still fails, rethrow the exception.
                if ($i === $retryCount - 1) {
                    throw $e;
                }
            }
        }
    }
}
