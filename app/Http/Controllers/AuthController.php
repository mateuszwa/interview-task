<?php

namespace App\Http\Controllers;

use App\Adapters\BarAuthAdapter;
use App\Adapters\BazAuthAdapter;
use App\Adapters\FooAuthAdapter;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends Controller
{
    // The login method authenticates a user and returns a JWT token.
    public function login(Request $request): JsonResponse
    {
        // Retrieve login and password from the request.
        $login = $request->input('login');
        $password = $request->input('password');

        // Determine the company from the login prefix
        $prefix = explode('_', $login)[0];

        $authService = null;
        // Choose the appropriate authentication service based on the prefix
        switch ($prefix) {
            case 'FOO':
                $authService = new FooAuthAdapter();
                break;
            case 'BAR':
                $authService = new BarAuthAdapter();
                break;
            case 'BAZ':
                $authService = new BazAuthAdapter();
                break;
            default:
                // If the prefix is not recognized, return a failure response.
                return response()->json(['status' => 'failure']);
        }

        // Authenticate the user
        if ($authService->authenticateUser($login, $password)) {
            // If authentication is successful, generate a JWT token.
            $token = JWT::encode([
                'login' => $login,
                'system' => $prefix,
                'iat' => time(),
            ], config('app.jwt_secret'), 'HS256');

            // Return the token in a success response.
            return response()->json([
                'status' => 'success',
                'token' => $token,
            ]);
        } else {
            // If authentication fails, return a failure response.
            return response()->json(['status' => 'failure']);
        }
    }
}
