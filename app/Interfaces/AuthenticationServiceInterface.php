<?php

namespace App\Interfaces;

interface AuthenticationServiceInterface
{
    public function authenticateUser(string $login, string $password): bool;
}
