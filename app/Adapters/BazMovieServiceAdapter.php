<?php

namespace App\Adapters;

use External\Baz\Movies\MovieService as BazMovieService;

class BazMovieServiceAdapter
{
    private $bazMovieService;

    public function __construct(BazMovieService $bazMovieService)
    {
        $this->bazMovieService = $bazMovieService;
    }

    public function getTitles(): array
    {
        $titles = $this->bazMovieService->getTitles();

        // Normalize the titles into a consistent format
        $normalizedTitles = array_map(function ($title) {
            return $title;
        }, $titles['titles']);

        return $normalizedTitles;
    }
}
