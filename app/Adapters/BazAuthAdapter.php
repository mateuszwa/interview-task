<?php

namespace App\Adapters;

use App\Interfaces\AuthenticationServiceInterface;
use External\Baz\Auth\Authenticator;
use External\Baz\Auth\Responses\Success;

class BazAuthAdapter implements AuthenticationServiceInterface
{
    protected Authenticator $authService;

    public function __construct()
    {
        $this->authService = new Authenticator();
    }

    public function authenticateUser(string $login, string $password): bool
    {
        $response = $this->authService->auth($login, $password);
        return $response instanceof Success;
    }
}
