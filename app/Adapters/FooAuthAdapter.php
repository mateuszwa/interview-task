<?php

namespace App\Adapters;

use App\Interfaces\AuthenticationServiceInterface;
use External\Foo\Auth\AuthWS;
use External\Foo\Exceptions\AuthenticationFailedException;

class FooAuthAdapter implements AuthenticationServiceInterface
{
    protected AuthWS $authService;

    public function __construct()
    {
        $this->authService = new AuthWS();
    }

    public function authenticateUser(string $login, string $password): bool
    {
        try {
            $this->authService->authenticate($login, $password);
            return true;
        } catch (AuthenticationFailedException $e) {
            return false;
        }
    }
}
