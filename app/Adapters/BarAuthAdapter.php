<?php

namespace App\Adapters;

use App\Interfaces\AuthenticationServiceInterface;
use External\Bar\Auth\LoginService;

class BarAuthAdapter implements AuthenticationServiceInterface
{
    protected LoginService $authService;

    public function __construct()
    {
        $this->authService = new LoginService();
    }

    public function authenticateUser(string $login, string $password): bool
    {
        return $this->authService->login($login, $password);
    }
}
