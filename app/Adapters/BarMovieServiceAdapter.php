<?php

namespace App\Adapters;

use External\Bar\Movies\MovieService as BarMovieService;

class BarMovieServiceAdapter
{
    private $barMovieService;

    public function __construct(BarMovieService $barMovieService)
    {
        $this->barMovieService = $barMovieService;
    }

    public function getTitles(): array
    {
        $titles = $this->barMovieService->getTitles();

        // Normalize the titles into a consistent format
        $normalizedTitles = array_map(function ($title) {
            return $title['title'];
        }, $titles['titles']);

        return $normalizedTitles;
    }
}
