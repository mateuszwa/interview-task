<?php

namespace App\Adapters;

use External\Foo\Movies\MovieService as FooMovieService;

class FooMovieServiceAdapter
{
    private $fooMovieService;

    public function __construct(FooMovieService $fooMovieService)
    {
        $this->fooMovieService = $fooMovieService;
    }

    public function getTitles(): array
    {
        $titles = $this->fooMovieService->getTitles();

        // Normalize the titles into a consistent format
        $normalizedTitles = array_map(function ($title) {
            return $title;
        }, $titles);

        return $normalizedTitles;
    }
}
